%global srcname entrypoints
%global sum Discover and load entry points from installed packages

Name:		python-%{srcname}
Version:	0.2.3
Release:	2%{?dist}
Summary:	%{sum}

# license clarification issue opened upstream
# https://github.com/takluyver/entrypoints/issues/10

License:	MIT

URL:		https://entrypoints.readthedocs.io/
Source0:	https://github.com/takluyver/%{srcname}/archive/%{version}/%{srcname}-%{version}.tar.gz

BuildArch:	noarch
BuildRequires:	python2-devel
BuildRequires:	python3-devel
BuildRequires:	python-pip
BuildRequires:	python3-pip
BuildRequires:	python3-flit
BuildRequires:	python3-sphinx

%description
Entry points are a way for Python packages to advertise objects with some
common interface. The most common examples are console_scripts entry points,
which define shell commands by identifying a Python function to run.

The entrypoints module contains functions to find and load entry points.


%package -n python2-%{srcname}
Summary:	%{sum}
%{?python_provide:%python_provide python2-%{srcname}}

%description -n python2-%{srcname}
Entry points are a way for Python packages to advertise objects with some
common interface. The most common examples are console_scripts entry points,
which define shell commands by identifying a Python function to run.

The entrypoints module contains functions to find and load entry points.


%package -n python3-%{srcname}
Summary:	%{sum}
%{?python_provide:%python_provide python3-%{srcname}}

%description -n python3-%{srcname}
Entry points are a way for Python packages to advertise objects with some
common interface. The most common examples are console_scripts entry points,
which define shell commands by identifying a Python function to run.

The entrypoints module contains functions to find and load entry points.

%package -n python-%{srcname}-doc
Summary:	Documentation for python-entrypoints

%description -n python-%{srcname}-doc
Documentation files for python-entrypoints

%prep
%autosetup -n %{srcname}-%{version}

# We don't need to verify PyPI classifiers, because the package is already
# there. So just make specified classifiers "valid".
mkdir -p .cache/flit
%{__python3} -c "with open('flit.ini', 'rb') as inf:
    with open('.cache/flit/classifiers.lst', 'wb') as outf:
        enable_output = False
        for line in inf:
            if enable_output:
                if b'=' in line:
                    break
                outf.write(line)
            elif b'classifiers' in line:
                outf.write(line.split(b'=')[1])
                enable_output = True
"


%build
XDG_CACHE_HOME=$PWD/.cache flit wheel

pushd doc
make html PYTHON="%{__python3}" SPHINXBUILD=sphinx-build-%{python3_version}
rm _build/html/.buildinfo
popd


%install
pip2 install --root %{buildroot} --no-deps \
	dist/%{srcname}-%{version}-py2.py3-none-any.whl
pip3 install --root %{buildroot} --no-deps \
	dist/%{srcname}-%{version}-py2.py3-none-any.whl


%files -n python2-%{srcname}
%doc doc/_build/html
%license LICENSE
%{python2_sitelib}/*.py*
%{python2_sitelib}/%{srcname}-%{version}.dist-info/

%files -n python3-%{srcname}
%doc doc/_build/html
%license LICENSE
%{python3_sitelib}/__pycache__/*
%{python3_sitelib}/*.py
%{python3_sitelib}/%{srcname}-%{version}.dist-info/

%files -n python-%{srcname}-doc
%doc doc/_build/html
%license LICENSE

%changelog
* Sun Jul 23 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> 0.2.3-2
- Update to latest version.

* Wed Jan 18 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> - 0.2.2-6
- Restore dist-info information

* Mon Dec 19 2016 Miro Hrončok <mhroncok@redhat.com> - 0.2.2-5
- Rebuild for Python 3.6

* Wed Nov 06 2016 Mukundan Ragavan <nonamedotc@fedoraproject.org> - 0.2.2-4
- Do not own pycache dir

* Mon Oct 03 2016 Mukundan Ragavan <nonamedotc@fedoraproject.org> - 0.2.2-3
- Consolidate two doc subpackages into one

* Sun Oct 02 2016 Mukundan Ragavan <nonamedotc@fedoraproject.org> - 0.2.2-2
- Add -doc subpackages
- Fix source url
- Add license clarification issue URL for reference

* Sat Jul 2 2016 Elliott Sales de Andrade <quantum.analyst@gmail.com> 0.2.2-1
- Initial RPM release
