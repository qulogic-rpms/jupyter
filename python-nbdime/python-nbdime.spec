%global srcname nbdime

Name:           python-%{srcname}
Version:        0.3.0
Release:        2%{?dist}
Summary:        Jupyter Notebook Diff and Merge tools

License:        BSD
URL:            https://pypi.python.org/pypi/%{srcname}
Source0:        https://github.com/jupyter/%{srcname}/archive/%{version}/%{srcname}-%{version}.tar.gz

BuildArch:      noarch
BuildRequires:  python3-devel
BuildRequires:  python3-six
BuildRequires:  python3-nbformat
BuildRequires:  python3-colorama
BuildRequires:  python3-tornado
BuildRequires:  python3-requests

BuildRequires:  python3-pytest
BuildRequires:  python3-pytest-tornado
BuildRequires:  python3-jsonschema
BuildRequires:  python3-tabulate
BuildRequires:  git
BuildRequires:  npm

BuildRequires:  python3-sphinx
BuildRequires:  python3-recommonmark
BuildRequires:  python3-sphinx_rtd_theme

Requires:       python3-six
Requires:       python3-nbformat
Requires:       python3-colorama
Requires:       python3-tornado
Requires:       python3-requests

%description
nbdime provides tools for diffing and merging of Jupyter Notebooks.

 * nbdiff compare notebooks in a terminal-friendly way
 * nbmerge three-way merge of notebooks with automatic conflict resolution
 * nbdiff-web shows you a rich rendered diff of notebooks
 * nbmerge-web gives you a web-based three-way merge tool for notebooks
 * nbshow present a single notebook in a terminal-friendly way


%prep
%autosetup -n %{srcname}-%{version} -Sgit


%build
%py3_build

pushd docs
make html PYTHON="%{__python3}" SPHINXBUILD=sphinx-build-%{python3_version}
rm build/html/.buildinfo
popd


%install
%py3_install


%check
GIT_AUTHOR_NAME='Bob the Builder' \
GIT_COMMITTER_NAME='Bob the Builder' \
GIT_AUTHOR_EMAIL='bob.builder@example.com' \
GIT_COMMITTER_EMAIL='bob.builder@example.com' \
PYTHONPATH="%{buildroot}%{python3_sitelib}" \
PATH="%{buildroot}%{_bindir}:$PATH" \
    py.test-%{python3_version} -ra


%files
%license LICENSE.md
%doc README.md docs/build/html
%{_bindir}/nbdime
%{_bindir}/nbdiff
%{_bindir}/nbdiff-web
%{_bindir}/nbmerge
%{_bindir}/nbmerge-web
%{_bindir}/nbshow
%{_bindir}/git-nbdiffdriver
%{_bindir}/git-nbdifftool
%{_bindir}/git-nbmergedriver
%{_bindir}/git-nbmergetool
%{python3_sitelib}/%{srcname}
%{python3_sitelib}/%{srcname}-%{version}-py?.?.egg-info


%changelog
* Mon Jul 24 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> 0.3.0-2
- Add npm BuildRequires.

* Mon Jul 24 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> 0.3.0-1
- Update to latest release.

* Sat Mar 04 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> 0.2.0-1
- Initial package release.
