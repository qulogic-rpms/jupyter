%global modname	ipykernel

Name:		python-%{modname}
Version:	4.6.1
Release:	2%{?dist}
Summary:	IPython Kernel for Jupyter
License:	BSD
URL:		https://github.com/ipython/%{modname}
Source0:	https://github.com/ipython/%{modname}/archive/%{version}/%{modname}-%{version}.tar.gz
BuildArch:	noarch
BuildRequires:	python-sphinx

%global _description \
This package provides the IPython kernel for Jupyter.

%description	%{_description}

%package -n	python2-%{modname}
Summary:	%{summary}
%{?python_provide:%python_provide python2-%{modname}}

BuildRequires:	python2-devel
BuildRequires:  python2-ipython >= 4.0.0
BuildRequires:  python2-traitlets >= 4.1.0
BuildRequires:  python2-jupyter-client
BuildRequires:  python2-tornado >= 4.0

Requires:       python2-ipython >= 4.0.0
Requires:       python2-traitlets >= 4.1.0
Requires:       python2-jupyter-client
Requires:       python2-tornado >= 4.0

%description -n	python2-%{modname} %{_description}

Python 2 version.

%package -n	python3-%{modname}
Summary:	%{summary}
%{?python_provide:%python_provide python3-%{modname}}

BuildRequires:	python3-devel
BuildRequires:  python3-ipython >= 4.0.0
BuildRequires:  python3-traitlets >= 4.1.0
BuildRequires:  python3-jupyter-client
BuildRequires:  python3-tornado >= 4.0

Requires:       python3-ipython >= 4.0.0
Requires:       python3-traitlets >= 4.1.0
Requires:       python3-jupyter-client
Requires:       python3-tornado >= 4.0

%description -n	python3-%{modname} %{_description}

Python 3 version.

%package	doc
Summary:	Documentation for %{name}

%description	doc
This package contains the documentation of %{name}.

%prep
%setup -q -n %{modname}-%{version}

%build
%py2_build
%py3_build

pushd docs
    make %{?_smp_mflags} html
popd

%install
%py2_install
%py3_install
mkdir -p %{buildroot}%{_docdir}/%{name}
cp -fpavr docs/_build/html %{buildroot}%{_docdir}/%{name}
rm %{buildroot}%{_docdir}/%{name}/html/{.buildinfo,objects.inv}

%files	-n python2-%{modname}
%license COPYING.md
%doc CONTRIBUTING.md README.md
%{python2_sitelib}/%{modname}
%{python2_sitelib}/%{modname}_launcher.py*
%{python2_sitelib}/%{modname}*.egg-info
%{_datadir}/jupyter/kernels/python2/

%files	-n python3-%{modname}
%license COPYING.md
%doc CONTRIBUTING.md README.md
%{python3_sitelib}/%{modname}
%{python3_sitelib}/%{modname}_launcher.py
%{python3_sitelib}/__pycache__/%{modname}_launcher.*.pyc
%{python3_sitelib}/%{modname}*.egg-info
%{_datadir}/jupyter/kernels/python3/

%files	doc
%doc %{_docdir}/%{name}/html

%changelog
* Mon Jul 24 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> 4.6.1-2
- Add new files to install.
- Remove extra files.
- Add proper BuildRequires.

* Mon Jul 24 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> 4.6.1-1
- Update to latest version.

* Sat Jan 07 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> - 4.5.2-4
- Add correct dependencies.

* Tue Dec 27 2016 pcpa <paulo.cesar.pereira.de.andrade@gmail.com> - 4.5.2-3
- Preseve timestamp of installed files (#1406958#c7)

* Mon Dec 26 2016 pcpa <paulo.cesar.pereira.de.andrade@gmail.com> - 4.5.2-2
- Use proper Source0 format (#1406958#c4)
- Do parallel html make (#1406958#c4)

* Thu Dec 22 2016 pcpa <paulo.cesar.pereira.de.andrade@gmail.com> - 4.5.2-1
- Update to latest upstream release.
- Correct removal of unnecessary doc files.

* Wed Nov 16 2016 pcpa <paulo.cesar.pereira.de.andrade@gmail.com> - 4.5.1-1
- Initial package.
