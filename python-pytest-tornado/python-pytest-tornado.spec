%global srcname pytest-tornado
%global srcname_ pytest_tornado

Name:           python-%{srcname}
Version:        0.4.5
Release:        3%{?dist}
Summary:        Py.test plugin for testing of asynchronous tornado applications

License:        ASL 2.0
URL:            https://pypi.python.org/pypi/%{srcname}
Source0:        https://github.com/eugeniy/%{srcname}/archive/v%{version}/%{srcname}-%{version}.tar.gz

BuildArch:      noarch

%global _description \
A py.test plugin providing fixtures and markers to simplify testing of \
asynchronous tornado applications.

%description %{_description}


%package -n python2-%{srcname}
Summary:        %{summary}
%{?python_provide:%python_provide python2-%{srcname}}

BuildRequires:  python2-devel
BuildRequires:  python2-pytest
BuildRequires:  python2-tornado

Requires:       python2-pytest
Requires:       python2-tornado

%description -n python2-%{srcname} %{_description}


%package -n python3-%{srcname}
Summary:        %{summary}
%{?python_provide:%python_provide python3-%{srcname}}

BuildRequires:  python3-devel
BuildRequires:  python3-pytest
BuildRequires:  python3-tornado

Requires:       python3-pytest
Requires:       python3-tornado

%description -n python3-%{srcname} %{_description}


%prep
%autosetup -n %{srcname}-%{version}


%build
%py2_build
%py3_build


%install
%py2_install
%py3_install


%check
PYTHONPATH="%{buildroot}%{python2_sitelib}" \
    py.test
PYTHONPATH="%{buildroot}%{python3_sitelib}" \
    py.test-%{python3_version}


%files -n python2-%{srcname}
%license LICENSE
%doc README.rst
%{python2_sitelib}/%{srcname_}
%{python2_sitelib}/%{srcname_}-%{version}-py?.?.egg-info


%files -n python3-%{srcname}
%license LICENSE
%doc README.rst
%{python3_sitelib}/%{srcname_}
%{python3_sitelib}/%{srcname_}-%{version}-py?.?.egg-info


%changelog
* Mon Oct 30 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> 0.4.5-3
- Use version tag instead of unnecessary commit archive.

* Sun Oct 29 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> 0.4.5-2
- Simplify spec against latest template.

* Sat Mar 04 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> 0.4.5-1
- Initial package release.
