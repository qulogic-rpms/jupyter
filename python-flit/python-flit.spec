# These require packages that are built using this package itself.
%global with_loop 0
%global with_test 0

%global python3_wheelname %{srcname}-%{version}-py3-none-any.whl

%global srcname flit
%global sum  Simplified packaging of Python modules

Name:		python-%{srcname}
Version:	0.11.3
Release:	2%{?dist}
Summary:	%{sum}

# ./flit/logo.py  under ASL 2.0 license
# ./flit/upload.py under PSF license
License:	BSD and ASL 2.0 and Python

URL:		https://flit.readthedocs.io/en/latest/
Source0:	https://github.com/takluyver/flit/archive/%{version}/%{srcname}-%{version}.tar.gz

Patch0:		%{srcname}-file-encoding.patch

BuildArch:	noarch
BuildRequires:	python3-devel
BuildRequires:	python3-pip
BuildRequires:	python3-requests
BuildRequires:	python3-docutils
BuildRequires:	python3-pypandoc
BuildRequires:	python3-pygments
%if 0%{?fedora} < 26
BuildRequires:	python3-zipfile36
%endif

%if 0%{?with_test}
BuildRequires:	python3-pytest
BuildRequires:	python3-responses

# Requires flit to build:
BuildRequires:	python3-testpath
%endif

# https://pypi.python.org/pypi/tornado
# ./flit/logo.py unkown version
Provides:    bundled(python-tornado)

%description
Flit is a simple way to put Python packages and modules on PyPI.

Flit only creates packages in the new 'wheel' format. People using older
versions of pip (<1.5) or easy_install will not be able to install them.

Flit packages a single importable module or package at a time, using the import
name as the name on PyPI. All sub-packages and data files within a package are
included automatically.

Flit requires Python 3, but you can use it to distribute modules for Python 2,
so long as they can be imported on Python 3.


%package -n python3-%{srcname}
Summary:	%{sum}
%{?python_provide:%python_provide python3-%{srcname}}

Requires:	python3-requests
Requires:	python3-docutils
Requires:	python3-pypandoc
%if 0%{?fedora} < 26
Requires:	python3-zipfile36
%endif
%if 0%{?with_loop}
Requires:	python3-requests-download
%endif

%description -n python3-%{srcname}
Flit is a simple way to put Python packages and modules on PyPI.

Flit only creates packages in the new 'wheel' format. People using older
versions of pip (<1.5) or easy_install will not be able to install them.

Flit packages a single importable module or package at a time, using the import
name as the name on PyPI. All subpackages and data files within a package are
included automatically.

Flit requires Python 3, but you can use it to distribute modules for Python 2,
so long as they can be imported on Python 3.


%prep
%autosetup -n %{srcname}-%{version} -p1

%build
export FLIT_NO_NETWORK=1
XDG_CACHE_HOME=$PWD/.cache %{__python3} -m flit wheel


%install
%py3_install_wheel %{python3_wheelname}

# Fedora doesn't use this yet.
rm -r %{buildroot}%{python3_sitelib}/%{srcname}-%{version}.dist-info/


%if 0%{?with_test}
%check
py.test-3
%endif


%files -n python3-%{srcname}
%license LICENSE
%doc README.rst
%{python3_sitelib}/*
%{_bindir}/flit


%changelog
* Sun Jul 23 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> 0.11.3-2
- Update to latest release.

* Sun Jul 09 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> 0.11.1-2
- Add zipfile36 to requirements.

* Tue Jun 13 2017 Mukundan Ragavan <nonamedotc@fedoraproject.org> - 0.11.1-1
- Update to 0.11.1

* Sat Feb 11 2017 Fedora Release Engineering <releng@fedoraproject.org> - 0.9-6
- Rebuilt for https://fedoraproject.org/wiki/Fedora_26_Mass_Rebuild

* Mon Jan 23 2017 Michal Cyprian <mcyprian@redhat.com> - 0.9-5
- Use python install wheel macro

* Mon Dec 19 2016 Miro Hrončok <mhroncok@redhat.com> - 0.9-4
- Rebuild for Python 3.6

* Thu Sep 29 2016 Mukundan Ragavan <nonamedotc@gmail.com> - 0.9-3
- Updated spec file with license comments and provides

* Sat Sep 24 2016 Mukundan Ragavan <nonamedotc@fedoraproject.org> - 0.9-2
- spec file cleanup

* Sat Jul 2 2016 Elliott Sales de Andrade <quantum.analyst@gmail.com> 0.9-1
- Initial RPM release
