# Created by pyp2rpm-3.0.1
%global pypi_name jupyter_client
%global pypi_name_dash jupyter-client
%global python2_wheelname %{pypi_name}-%{version}-py2.py3-none-any.whl
%global python3_wheelname %python2_wheelname

Name:           python-%{pypi_name_dash}
Version:        5.1.0
Release:        2%{?dist}
Summary:        Jupyter protocol implementation and client libraries

License:        BSD
URL:            http://jupyter.org
Source0:        https://files.pythonhosted.org/packages/source/j/%{pypi_name}/%{pypi_name}-%{version}.tar.gz
BuildArch:      noarch

BuildRequires:  python2-setuptools
BuildRequires:  python2-devel
BuildRequires:  python2-wheel
BuildRequires:  python2-traitlets
BuildRequires:  python2-jupyter-core
%if 0%{?fedora} >= 25
BuildRequires:  python2-zmq >= 13
%else
BuildRequires:  python-zmq >= 13
%endif
BuildRequires:  python2-dateutil >= 2.1

BuildRequires:  python3-setuptools
BuildRequires:  python3-devel
BuildRequires:  python3-wheel

%if 0%{?fedora} >= 25
BuildRequires:  python-sphinx
BuildRequires:  python-sphinxcontrib-napoleon
%endif

%description
This package contains the reference implementation of the Jupyter protocol.
It also provides client and kernel management APIs for working with kernels.

It also provides the `jupyter kernelspec` entrypoint for installing kernelspecs
for use with Jupyter frontends.

%package -n     python2-%{pypi_name_dash}
Summary:        Jupyter protocol implementation and client libraries
%{?python_provide:%python_provide python2-%{pypi_name_dash}}

Requires:       python2-traitlets
Requires:       python2-jupyter-core
%if 0%{?fedora} >= 25
Requires:       python2-zmq >= 13
%else
Requires:       python-zmq >= 13
%endif
Requires:       python2-dateutil >= 2.1
Requires:       python2-setuptools
%description -n python2-%{pypi_name_dash}
This package contains the reference implementation of the Jupyter protocol.
It also provides client and kernel management APIs for working with kernels.

%package -n     python3-%{pypi_name_dash}
Summary:        Jupyter protocol implementation and client libraries
%{?python_provide:%python_provide python3-%{pypi_name_dash}}

Requires:       python3-traitlets
Requires:       python3-jupyter-core
Requires:       python3-zmq >= 13
Requires:       python3-dateutil >= 2.1
Requires:       python3-setuptools
%description -n python3-%{pypi_name_dash}
This package contains the reference implementation of the Jupyter protocol.
It also provides client and kernel management APIs for working with kernels.

It also provides the `jupyter kernelspec` entrypoint for installing kernelspecs
for use with Jupyter frontends.

%package -n python-%{pypi_name_dash}-doc
Summary:        Documentation of the Jupyter protocol reference implementation
%description -n python-%{pypi_name_dash}-doc
Documentation of the reference implementation of the Jupyter protocol

%prep
%autosetup -n %{pypi_name}-%{version}

%build
%py2_build_wheel
%py3_build_wheel
%if 0%{?fedora} >= 25
PYTHONPATH=build/lib/ sphinx-build docs html
# remove the sphinx-build leftovers
rm -r html/.{doctrees,buildinfo}
%endif


%install
# Must do the subpackages' install first because the scripts in /usr/bin are
# overwritten with every setup.py install.
%py2_install_wheel %{python2_wheelname}
%py3_install_wheel %{python3_wheelname}

%global _docdir_fmt %{name}

%files -n python2-%{pypi_name_dash}
%doc README.md
%license COPYING.md
%{python2_sitelib}/%{pypi_name}-%{version}.dist-info
%{python2_sitelib}/%{pypi_name}/

%files -n python3-%{pypi_name_dash}
%doc README.md
%license COPYING.md
%{_bindir}/jupyter-kernelspec
%{_bindir}/jupyter-run
%{python3_sitelib}/%{pypi_name}-%{version}.dist-info
%{python3_sitelib}/%{pypi_name}/

%files -n python-%{pypi_name_dash}-doc
%if 0%{?fedora} >= 25
%doc html
%endif

%changelog
* Mon Jul 24 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> 5.1.0-2
- Only provide Python 3 executables.
- Add new executable to file list.
- Use wheel when building.

* Mon Jul 24 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> 5.1.0-1
- Update to latest version.

* Mon Jun 05 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> - 5.0.1-1
- New upstream release

* Thu Mar 02 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> - 5.0.0-1
- New upstream release

* Wed Jan 04 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> - 4.4.0-2
- Better compatibility with older releases

* Mon Dec 19 2016 Miro Hrončok <mhroncok@redhat.com> - 4.4.0-2
- Rebuild for Python 3.6

* Mon Sep 26 2016 Thomas Spura <tomspur@fedoraproject.org> - 4.4.0-1
- update to 4.4.0
- Source0: use files.pythonhosted.org

* Mon Apr 25 2016 Thomas Spura <tomspur@fedoraproject.org> - 4.2.2-4
- Use simpler docdir_fmt
- Fix BR/R requires

* Tue Apr 19 2016 Thomas Spura <tomspur@fedoraproject.org> - 4.2.2-3
- Fix docs generation (Zbigniew, #1327989)
- Require python2- instead python- where possible

* Mon Apr 18 2016 Thomas Spura <tomspur@fedoraproject.org> - 4.2.2-2
- Use dash in name
- Adjust description
- Use %%license

* Mon Apr 18 2016 Thomas Spura <tomspur@fedoraproject.org> - 4.2.2-1
- Initial package.
