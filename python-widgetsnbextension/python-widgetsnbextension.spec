# Created by pyp2rpm-3.2.1
%global pypi_name widgetsnbextension

Name:           python-%{pypi_name}
Version:        2.0.0
Release:        4%{?dist}
Summary:        IPython HTML widgets for Jupyter

License:        BSD
URL:            https://ipython.org
Source0:        https://files.pythonhosted.org/packages/source/w/%{pypi_name}/%{pypi_name}-%{version}.tar.gz
BuildArch:      noarch

BuildRequires:  python2-devel
BuildRequires:  python-setuptools
# For extension installation.
BuildRequires:  python2-notebook >= 4.4.1-2

BuildRequires:  python3-devel
BuildRequires:  python3-setuptools
# For extension installation.
BuildRequires:  python3-notebook >= 4.4.1-2

%description
Interactive HTML widgets for Jupyter notebooks.


%package -n     python2-%{pypi_name}
Summary:        IPython HTML widgets for Jupyter
%{?python_provide:%python_provide python2-%{pypi_name}}

Conflicts:      python-%{pypi_name}-js < 2.0.0-3
Requires:       python2-notebook >= 4.4.1-2
Enhances:       python2-notebook

%description -n python2-%{pypi_name}
Interactive HTML widgets for Jupyter notebooks.


%package -n     python3-%{pypi_name}
Summary:        IPython HTML widgets for Jupyter
%{?python_provide:%python_provide python3-%{pypi_name}}

Conflicts:      python-%{pypi_name}-js < 2.0.0-3
Requires:       python3-notebook >= 4.4.1-2
Enhances:       python3-notebook

%description -n python3-%{pypi_name}
Interactive HTML widgets for Jupyter notebooks.


%prep
%autosetup -n %{pypi_name}-%{version}
# Remove bundled egg-info
rm -rf %{pypi_name}.egg-info


%build
%py2_build
%py3_build


%install
# Must do the subpackages' install first because the scripts in /usr/bin are
# overwritten with every setup.py install.
%py2_install
%py3_install

# Equivalent to `jupyter-3 nbextension install --symlink` but without requiring
# the package to be installed already.
rm -rf %{buildroot}%{_datarootdir}/jupyter/nbextensions/jupyter-js-widgets
ln -s %{python3_sitelib}/%{pypi_name}/static %{buildroot}%{_datarootdir}/jupyter/nbextensions/jupyter-js-widgets


%post -n python3-%{pypi_name}
jupyter nbextension enable --sys-prefix --py widgetsnbextension
exit 0

%preun -n python3-%{pypi_name}
if [ $1 -gt 0 ]; then
jupyter nbextension disable --sys-prefix --py widgetsnbextension
fi
exit 0


%files -n python2-%{pypi_name}
%doc
%{python2_sitelib}/%{pypi_name}
%{python2_sitelib}/%{pypi_name}-%{version}-py?.?.egg-info

%files -n python3-%{pypi_name}
%doc
%{python3_sitelib}/%{pypi_name}
%{python3_sitelib}/%{pypi_name}-%{version}-py?.?.egg-info
%{_datarootdir}/jupyter/nbextensions/jupyter-js-widgets


%changelog
* Mon Jul 24 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> 2.0.0-4
- Remove Python 2 jupyter installation.
- Rename python-jupyter-notebook as python-notebook.

* Sat Mar 04 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> - 2.0.0-3
- Install notebook extension in new Python2 directory.
- Install notebook extension during RPM build; only do config update in post.
- Remove unnecessary -js subpackage.

* Fri Mar 03 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> - 2.0.0-2
- Don't remove notebook extension for an upgrade.

* Thu Mar 02 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> - 2.0.0-1
- First non-beta release.

* Fri Feb 17 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> - 2.0.0b19-5
- Really fix bug due to post-version suffix.

* Fri Feb 17 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> - 2.0.0b19-4
- Fix bug in setup.

* Fri Feb 17 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> - 2.0.0b19-3
- New upstream release.

* Mon Jan 16 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> - 2.0.0b9-2
- Install Jupyter notebook extension.

* Sun Jan 15 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> - 2.0.0b9-1
- New upstream release.

* Sun Jan 08 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> - 2.0.0b7-1
- Initial package.
