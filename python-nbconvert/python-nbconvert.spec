%global pypi_name nbconvert

# Doc build requires nbsphinx which needs nbconvert.
%bcond_without doc

Name:           python-%{pypi_name}
Version:        5.1.1
Release:        2%{?dist}
Summary:        Converting Jupyter Notebooks

License:        BSD and MIT
URL:            http://jupyter.org
Source0:        https://files.pythonhosted.org/packages/source/n/%{pypi_name}/%{pypi_name}-%{version}.tar.gz
BuildArch:      noarch

BuildRequires:  python-setuptools
BuildRequires:  python2-devel

BuildRequires:  python3-setuptools
BuildRequires:  python3-devel

%if %{with doc}
BuildRequires:  python3-sphinx
BuildRequires:  python3-nbsphinx
BuildRequires:  python3-ipython-sphinx
BuildRequires:  pandoc
BuildRequires:  python3-jinja2
BuildRequires:  python3-pygments
BuildRequires:  python3-traitlets >= 4.2
BuildRequires:  python3-jupyter-core
BuildRequires:  python3-jupyter-client
BuildRequires:  python3-nbformat
BuildRequires:  python3-entrypoints >= 0.2.2
BuildRequires:  python3-bleach
BuildRequires:  python3-pandocfilters >= 1.4.1
BuildRequires:  python3-testpath
%endif

%description
The nbconvert tool, jupyter nbconvert, converts notebooks to various other
formats via Jinja templates. The nbconvert tool allows you to convert an
.ipynb notebook file into various static formats including HTML, LaTeX,
PDF, Reveal JS, Markdown (md), ReStructured Text (rst) and executable script.

%package -n     python2-%{pypi_name}
Summary:        Converting Jupyter Notebooks
%{?python_provide:%python_provide python2-%{pypi_name}}

Conflicts:      python-mistune = 0.6
Requires:       python-mistune
Requires:       python-jinja2
Requires:       python-pygments
Requires:       python-traitlets > 4.2
Requires:       python2-jupyter-core
Requires:       python-nbformat
Requires:       python-entrypoints >= 0.2.2
Requires:       python2-bleach
Requires:       python2-pandocfilters >= 1.4.1
Requires:       python2-testpath
Requires:       python-setuptools
Recommends:     python2-jupyter-client
Recommends:     pandoc

%description -n python2-%{pypi_name}

The nbconvert tool, jupyter nbconvert, converts notebooks to various other
formats via Jinja templates. The nbconvert tool allows you to convert an
.ipynb notebook file into various static formats including HTML, LaTeX,
PDF, Reveal JS, Markdown (md), ReStructured Text (rst) and executable script.

%package -n     python3-%{pypi_name}
Summary:        Converting Jupyter Notebooks
%{?python_provide:%python_provide python3-%{pypi_name}}

Conflicts:      python3-mistune = 0.6
Requires:       python3-mistune
Requires:       python3-jinja2
Requires:       python3-pygments
Requires:       python3-traitlets >= 4.2
Requires:       python3-jupyter-core
Requires:       python3-nbformat
Requires:       python3-entrypoints >= 0.2.2
Requires:       python3-bleach
Requires:       python3-pandocfilters >= 1.4.1
Requires:       python3-testpath
Requires:       python3-setuptools
Recommends:     python3-jupyter-client
Recommends:     pandoc

%description -n python3-%{pypi_name}

The nbconvert tool, jupyter nbconvert, converts notebooks to various other
formats via Jinja templates. The nbconvert tool allows you to convert an
.ipynb notebook file into various static formats including HTML, LaTeX,
PDF, Reveal JS, Markdown (md), ReStructured Text (rst) and executable script.

%if %{with doc}
%package -n python-%{pypi_name}-doc
Summary:        Documentation for nbconvert
%description -n python-%{pypi_name}-doc
Documentation for nbconvert
%endif


%prep
%autosetup -n %{pypi_name}-%{version}
rm -rf %{pypi_name}.egg-info


%build
%py2_build
%py3_build

%if %{with doc}
# generate html docs
pushd docs
PYTHONPATH=../build/lib python3 autogen_config.py
sphinx-build-%{python3_version} source ../html
popd

# remove the sphinx-build leftovers
rm -rf html/.{doctrees,buildinfo}
%endif


%install

%py3_install
mv %{buildroot}/%{_bindir}/jupyter-nbconvert %{buildroot}/%{_bindir}/jupyter-nbconvert-3
ln -sf %{_bindir}/jupyter-nbconvert-3 %{buildroot}/%{_bindir}/jupyter-nbconvert-%{python3_version}

# fix permissions and shebangs
sed -i '1s=^#!/usr/bin/\(python\|env python\)[23]\?=#!%{__python3}=' %{buildroot}%{python3_sitelib}/%{pypi_name}/nbconvertapp.py
chmod 755 %{buildroot}%{python3_sitelib}/%{pypi_name}/nbconvertapp.py

%py2_install
cp %{buildroot}/%{_bindir}/jupyter-nbconvert %{buildroot}/%{_bindir}/jupyter-nbconvert-2
ln -sf %{_bindir}/jupyter-nbconvert-2 %{buildroot}/%{_bindir}/jupyter-nbconvert-%{python2_version}

# fix permissions and shebangs
sed -i '1s=^#!/usr/bin/\(python\|env python\)[23]\?=#!%{__python2}=' %{buildroot}%{python2_sitelib}/%{pypi_name}/nbconvertapp.py
chmod 755 %{buildroot}%{python2_sitelib}/%{pypi_name}/nbconvertapp.py


%files -n python2-%{pypi_name}
%license COPYING.md
%doc docs/README.md
%{_bindir}/jupyter-nbconvert
%{_bindir}/jupyter-nbconvert-2
%{_bindir}/jupyter-nbconvert-%{python2_version}
%{python2_sitelib}/%{pypi_name}-%{version}-py?.?.egg-info
%{python2_sitelib}/%{pypi_name}/*

%files -n python3-%{pypi_name}
%license COPYING.md
%doc docs/README.md
%{_bindir}/jupyter-nbconvert-3
%{_bindir}/jupyter-nbconvert-%{python3_version}
%{python3_sitelib}/%{pypi_name}-%{version}-py?.?.egg-info
%{python3_sitelib}/%{pypi_name}/*

%if %{with doc}
%files -n python-%{pypi_name}-doc
%doc html
%endif


%changelog
* Sun Mar 12 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> - 5.1.1-2
- Add missing mistune dependency

* Fri Feb 17 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> - 5.1.1-1
- New upstream release

* Sun Jan 15 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> - 5.0.0-2
- Fix dependencies
- Add some weak dependencies

* Sun Jan 15 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> - 5.0.0-1
- New upstream release
- Fix buggy doc build

* Mon Dec 19 2016 Miro Hrončok <mhroncok@redhat.com> - 4.2.0-6
- Rebuild for Python 3.6

* Thu Nov 03 2016 Mukundan Ragavan <nonamedotc@fedoraproject.org> - 4.2.0-5
- Python dep chain fixed
- Fixes bug#1391124

* Wed Nov 02 2016 Mukundan Ragavan <nonamedotc@fedoraproject.org> - 4.2.0-4
- Fix pulling entire python{2,3} stack as deps
- Fixes bug#1391124

* Sun Oct 02 2016 Mukundan Ragavan <nonamedotc@fedoraproject.org> - 4.2.0-3
- Fix issues pointed out by rpmlint
- Fix license field

* Thu Aug 11 2016 Mukundan Ragavan <nonamedotc@fedoraproject.org> - 4.2.0-2
- Fix build errors

* Thu Aug 11 2016 Mukundan Ragavan <nonamedotc@gmail.com> - 4.2.0-1
- Initial package.
