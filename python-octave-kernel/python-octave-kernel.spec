%global srcname octave-kernel
%global srcname_ octave_kernel

Name:           python-%{srcname}
Version:        0.26.2
Release:        1%{?dist}
Summary:        A Jupyter kernel for Octave

License:        MIT
URL:            https://pypi.python.org/pypi/%{srcname}
Source0:        https://files.pythonhosted.org/packages/source/o/%{srcname_}/%{srcname_}-%{version}.tar.gz

BuildArch:      noarch

BuildRequires:  octave gnuplot

%global _description \
A Jupyter kernel for Octave.

%description %{_description}


%package -n python2-%{srcname}
Summary:        %{summary}
%{?python_provide:%python_provide python2-%{srcname}}

BuildRequires:  python2-devel
BuildRequires:  python2-metakernel >= 0.18.0
BuildRequires:  python2-jupyter-client >= 4.3.0
BuildRequires:  python2-ipykernel

Requires:       octave
Requires:       python2-metakernel >= 0.18.0
Requires:       python2-jupyter-client >= 4.3.0
Requires:       python2-ipykernel

%description -n python2-%{srcname} %{_description}


%package -n python3-%{srcname}
Summary:        %{summary}
%{?python_provide:%python_provide python3-%{srcname}}

BuildRequires:  python3-devel
BuildRequires:  python3-metakernel >= 0.18.0
BuildRequires:  python3-jupyter-client >= 4.3.0
BuildRequires:  python3-ipykernel

BuildRequires:  python3-jupyter-kernel-test

Requires:       octave
Requires:       python3-metakernel >= 0.18.0
Requires:       python3-jupyter-client >= 4.3.0
Requires:       python3-ipykernel

%description -n python3-%{srcname} %{_description}


%prep
%autosetup -n %{srcname_}-%{version} -p1


%build
%py2_build
%py3_build


%install
%py2_install
%{__python2} -m octave_kernel.install --prefix="%{buildroot}%{_prefix}"
mv "%{buildroot}%{_datarootdir}/jupyter" "%{buildroot}%{_datarootdir}/jupyter2"

%py3_install
%{__python3} -m octave_kernel.install --prefix="%{buildroot}%{_prefix}"


%check
PYTHONPATH="%{buildroot}%{python3_sitelib}" \
    JUPYTER_PATH="%{buildroot}%{_datarootdir}/jupyter" \
        %{__python3} test_octave_kernel.py -v


%files -n python2-%{srcname}
%license LICENSE.txt
%doc README.rst
%{python2_sitelib}/%{srcname_}
%{python2_sitelib}/%{srcname_}-%{version}-py?.?.egg-info
%{_datarootdir}/jupyter2/kernels/octave


%files -n python3-%{srcname}
%license LICENSE.txt
%doc README.rst
%{python3_sitelib}/%{srcname_}
%{python3_sitelib}/%{srcname_}-%{version}-py?.?.egg-info
%{_datarootdir}/jupyter/kernels/octave


%changelog
* Mon Jun 5 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> 0.26.2-1
- New upstream release.

* Sun Mar 12 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> 0.26.0-1
- Initial package release.
