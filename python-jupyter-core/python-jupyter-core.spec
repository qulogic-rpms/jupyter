# Created by pyp2rpm-3.0.1
%global pypi_name jupyter_core
%global pypi_name_dash jupyter-core

Name:           python-%{pypi_name_dash}
Version:        4.3.0
Release:        3%{?dist}
Summary:        The base package for Jupyter projects

License:        BSD
URL:            https://jupyter.org
Source0:        https://pypi.python.org/packages/source/j/%{pypi_name}/%{pypi_name}-%{version}.tar.gz
BuildArch:      noarch

BuildRequires:  python-setuptools
BuildRequires:  python2-devel
BuildRequires:  python-sphinx

BuildRequires:  python%{python3_pkgversion}-setuptools
BuildRequires:  python%{python3_pkgversion}-devel
# Docs built with python2 sphinx for now
#BuildRequires:  python%{python3_pkgversion}-sphinx

%description
Core common functionality of Jupyter projects.

This package contains base application classes and configuration inherited by
other projects.

%package -n     python2-%{pypi_name_dash}
Summary:        The base package for Jupyter projects
%{?python_provide:%python_provide python2-%{pypi_name_dash}}

# Provide / Obsolete existing python-jupyter_core
Obsoletes:      python2-%{pypi_name} < 4.0.2-3
Provides:       python2-%{pypi_name} = %{version}-%{release}
%{?python_provide:%python_provide python2-%{pypi_name}}

Requires:       python-traitlets
Requires:       python-setuptools
%description -n python2-%{pypi_name_dash}
Core common functionality of Jupyter projects.

This package contains base application classes and configuration inherited by
other projects.

%package -n     python%{python3_pkgversion}-%{pypi_name_dash}
Summary:        The base package for Jupyter projects
%{?python_provide:%python_provide python%{python3_pkgversion}-%{pypi_name_dash}}

Obsoletes:      python%{python3_pkgversion}-%{pypi_name} < 4.0.2-3
Provides:       python%{python3_pkgversion}-%{pypi_name} = %{version}-%{release}

Requires:       python%{python3_pkgversion}-traitlets
Requires:       python%{python3_pkgversion}-setuptools
%description -n python%{python3_pkgversion}-%{pypi_name_dash}
Core common functionality of Jupyter projects.

This package contains base application classes and configuration inherited by
other projects.

%package -n python-%{pypi_name_dash}-doc
Summary:        Documentation of the base package for Jupyter projects
%description -n python-%{pypi_name_dash}-doc
Core common functionality of Jupyter projects.

This package contains documentation for the base application classes and
configuration inherited by other jupyter projects.

%prep
%autosetup -n %{pypi_name}-%{version}

%build
%py2_build
%py3_build
# generate html docs
PYTHONPATH=build/lib/ sphinx-build docs html
# remove the sphinx-build leftovers
rm -rf html/.{doctrees,buildinfo}

%install
find | grep pyc$ | xargs rm -v
# Must do the subpackages' install first because the scripts in /usr/bin are
# overwritten with every setup.py install.
%py2_install
%py3_install

# Remove shebang from troubleshoot.py
for lib in %{buildroot}{%{python2_sitelib},%{python3_sitelib}}/jupyter_core/troubleshoot.py; do
    sed '1{\@^#!/usr/bin/env@d}' $lib > $lib.new &&
    touch -r $lib $lib.new &&
    mv $lib.new $lib
done

mkdir -p %{buildroot}%{_sysconfdir}/jupyter
mkdir -p %{buildroot}%{_datarootdir}/jupyter


%global _docdir_fmt %{name}

%files -n python2-%{pypi_name_dash}
%license COPYING.md
%doc README.md
%{python2_sitelib}/jupyter.py*
%{python2_sitelib}/%{pypi_name}-%{version}-py?.?.egg-info
%{python2_sitelib}/%{pypi_name}/

%files -n python%{python3_pkgversion}-%{pypi_name_dash}
%license COPYING.md
%doc README.md
%{_bindir}/jupyter
%{_bindir}/jupyter-migrate
%dir %{_datarootdir}/jupyter
%dir %{_sysconfdir}/jupyter
%{python3_sitelib}/__pycache__/*
%{python3_sitelib}/jupyter.py
%{python3_sitelib}/%{pypi_name}-%{version}-py?.?.egg-info
%{python3_sitelib}/%{pypi_name}/

%files -n python-%{pypi_name_dash}-doc
%doc html

%changelog
* Mon Jul 24 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> 4.3.0-3
- Stop shipping Python 2 executables.
- Fix broken changelog.

* Fri Mar 03 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> - 4.3.0-2
- Own extension directories

* Fri Feb 17 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> - 4.3.0-1
- New upstream release

* Sat Feb 11 2017 Fedora Release Engineering <releng@fedoraproject.org> - 4.1.0-8
- Rebuilt for https://fedoraproject.org/wiki/Fedora_26_Mass_Rebuild

* Mon Dec 19 2016 Miro Hrončok <mhroncok@redhat.com> - 4.1.0-7
- Rebuild for Python 3.6

* Wed Nov 16 2016 Orion Poplwski <orion@cora.nwra.com> - 4.1.0-6
- Do not own __pycache__ dir
- Enable EPEL builds

* Tue Jul 19 2016 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 4.1.0-5
- https://fedoraproject.org/wiki/Changes/Automatic_Provides_for_Python_RPM_Packages

* Sat Apr 23 2016 Thomas Spura <tomspur@fedoraproject.org> - 4.1.0-4
- Add obsoletes/provides for jupyter_core
- Fix python2 files installed with python3

* Mon Apr 18 2016 Thomas Spura <tomspur@fedoraproject.org> - 4.1.0-3
- Remove references to jupyter-troubleshoot
- Improve summary
- Remove shebang from troubleshoot.py

* Mon Apr 18 2016 Thomas Spura <tomspur@fedoraproject.org> - 4.1.0-2
- Add PYTHONPATH to sphinx-build (Zbigniew, #1327994)
- Install script differently (Zbigniew, #1327994)
- Rename packages to avoid underscore

* Mon Apr 18 2016 Thomas Spura <tomspur@fedoraproject.org> - 4.1.0-1
- Initial package.
