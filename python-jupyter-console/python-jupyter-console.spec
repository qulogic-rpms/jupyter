# Created by pyp2rpm-3.2.1
%global pypi_name jupyter_console
%global pypi_name_dash jupyter-console

Name:           python-%{pypi_name_dash}
Version:        5.1.0
Release:        1%{?dist}
Summary:        Jupyter terminal console

License:        BSD
URL:            https://jupyter.org
Source0:        https://files.pythonhosted.org/packages/source/j/%{pypi_name}/%{pypi_name}-%{version}.tar.gz
BuildArch:      noarch

BuildRequires:  python2-devel
BuildRequires:  python-setuptools
BuildRequires:  python-sphinx

BuildRequires:  python%{python3_pkgversion}-devel
BuildRequires:  python%{python3_pkgversion}-setuptools

%description
An IPythonlike terminal frontend for Jupyter kernels in any language.


%package -n     python2-%{pypi_name_dash}
Summary:        Jupyter terminal console
%{?python_provide:%python_provide python2-%{pypi_name}}

Requires:       python2-jupyter-client
Requires:       python2-ipython
Requires:       python2-ipykernel
Requires:       python2-prompt_toolkit < 2.0.0
Requires:       python2-pygments

%description -n python2-%{pypi_name_dash}
An IPythonlike terminal frontend for Jupyter kernels in any language.


%package -n     python%{python3_pkgversion}-%{pypi_name_dash}
Summary:        Jupyter terminal console
%{?python_provide:%python_provide python%{python3_pkgversion}-%{pypi_name_dash}}

Requires:       python%{python3_pkgversion}-jupyter-client
Requires:       python%{python3_pkgversion}-ipython
Requires:       python%{python3_pkgversion}-ipykernel
Requires:       python%{python3_pkgversion}-prompt_toolkit < 2.0.0
Requires:       python%{python3_pkgversion}-pygments

%description -n python%{python3_pkgversion}-%{pypi_name_dash}
An IPythonlike terminal frontend for Jupyter kernels in any language.


%package -n python-%{pypi_name_dash}-doc
Summary:        jupyter_console documentation
%description -n python-%{pypi_name_dash}-doc
Documentation for jupyter_console


%prep
%autosetup -n %{pypi_name}-%{version}


%build
%py2_build
%py3_build
# generate html docs
sphinx-build docs html
# remove the sphinx-build leftovers
rm -rf html/.{doctrees,buildinfo}


%install
# Must do the subpackages' install first because the scripts in /usr/bin are
# overwritten with every setup.py install.
%py3_install
cp %{buildroot}/%{_bindir}/jupyter-console %{buildroot}/%{_bindir}/jupyter-console-3
ln -sf %{_bindir}/jupyter-console-3 %{buildroot}/%{_bindir}/jupyter-console-%{python3_version}

%py2_install
cp %{buildroot}/%{_bindir}/jupyter-console %{buildroot}/%{_bindir}/jupyter-console-2
ln -sf %{_bindir}/jupyter-console-2 %{buildroot}/%{_bindir}/jupyter-console-%{python2_version}


#%check
#nosetests-%{python3_version} jupyter_console
#nosetests-%{python2_version} jupyter_console


%files -n python2-%{pypi_name_dash}
%license COPYING.md
%doc README.md
%{_bindir}/jupyter-console
%{_bindir}/jupyter-console-2
%{_bindir}/jupyter-console-%{python2_version}
%{python2_sitelib}/%{pypi_name}
%{python2_sitelib}/%{pypi_name}-%{version}-py?.?.egg-info


%files -n python%{python3_pkgversion}-%{pypi_name_dash}
%license COPYING.md
%doc README.md
%{_bindir}/jupyter-console-3
%{_bindir}/jupyter-console-%{python3_version}
%{python3_sitelib}/%{pypi_name}
%{python3_sitelib}/%{pypi_name}-%{version}-py?.?.egg-info


%files -n python-%{pypi_name_dash}-doc
%doc html


%changelog
* Fri Feb 17 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> - 5.1.0-1
- New upstream release

* Thu Jan 05 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> - 5.0.0-1
- Initial package.
