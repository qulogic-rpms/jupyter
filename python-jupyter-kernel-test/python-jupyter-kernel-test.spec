%global srcname jupyter-kernel-test
%global srcname_ jupyter_kernel_test

Name:           python-%{srcname}
Version:        0.3
Release:        4%{?dist}
Summary:        Machinery for testing Jupyter kernels via the messaging protocol

License:        BSD
URL:            https://pypi.python.org/pypi/%{srcname}
Source0:        https://github.com/jupyter/%{srcname_}/archive/%{version}/%{srcname}-%{version}.tar.gz

BuildArch:      noarch

# No support for Python 2.
BuildRequires:  python3-devel
BuildRequires:  python3-pip
BuildRequires:  python3-flit
BuildRequires:  python3-traitlets
BuildRequires:  python3-jupyter-client
BuildRequires:  python3-nose

BuildRequires:  python3-ipykernel

%description
jupyter_kernel_test is a tool for testing Jupyter kernels. It tests kernels for
successful code execution and conformance with the Jupyter Messaging Protocol
(currently 5.0).


%package -n python3-%{srcname}
Summary:        %{summary}
%{?python_provide:%python_provide python3-%{srcname}}

Requires:       python3-traitlets
Requires:       python3-jupyter-client
Requires:       python3-nose

%description -n python3-%{srcname}
jupyter_kernel_test is a tool for testing Jupyter kernels. It tests kernels for
successful code execution and conformance with the Jupyter Messaging Protocol
(currently 5.0).


%prep
%autosetup -n %{srcname_}-%{version}


%build
export FLIT_NO_NETWORK=1
XDG_CACHE_HOME=$PWD/.cache flit wheel


%install
%py3_install_wheel %{srcname_}-%{version}-py3-none-any.whl


%check
PYTHONPATH="%{buildroot}%{python3_sitelib}" \
    %{__python3} test_ipykernel.py


# Note that there is no %%files section for the unversioned python module if we
# are building for several python runtimes
%files -n python3-%{srcname}
%license COPYING.md
%doc README.rst
%{python3_sitelib}/%{srcname_}
%{python3_sitelib}/%{srcname_}-%{version}.dist-info


%changelog
* Mon Oct 30 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> 0.3-4
- Fix license tag.

* Sun Oct 29 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> 0.3-3
- Use new method of skipping flit network usage.
- Simplify spec against latest template.

* Mon Jul 24 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> 0.3-2
- Use new py3_install_wheel macro.

* Mon Jul 24 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> 0.3-1
- Update to latest version.

* Sun Mar 12 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> 0.2.2-1
- Initial package release.
