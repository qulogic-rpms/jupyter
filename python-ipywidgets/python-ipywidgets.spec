# Created by pyp2rpm-3.2.1
%global pypi_name ipywidgets
# recommonmark is broken at the moment.
%bcond_with doc

Name:           python-%{pypi_name}
Version:        6.0.0
Release:        1%{?dist}
Summary:        IPython HTML widgets for Jupyter

License:        BSD
URL:            https://ipython.org
Source0:        https://files.pythonhosted.org/packages/source/i/%{pypi_name}/%{pypi_name}-%{version}.tar.gz
BuildArch:      noarch

BuildRequires:  python2-devel
BuildRequires:  python-setuptools

%if %{with doc}
BuildRequires:  python3-sphinx
BuildRequires:  python3-recommonmark
%endif

BuildRequires:  python2-nose
BuildRequires:  python2-mock
BuildRequires:  python2-ipython >= 4.0.0
BuildRequires:  python2-ipykernel >= 4.5.1
BuildRequires:  python2-traitlets >= 4.3.1
BuildRequires:  python2-nbformat >= 4.2.0
# Required for traitlets tests
BuildRequires:  python2-pytest

BuildRequires:  python3-devel
BuildRequires:  python3-setuptools

BuildRequires:  python3-nose
BuildRequires:  python3-ipython >= 4.0.0
BuildRequires:  python3-ipykernel >= 4.5.1
BuildRequires:  python3-traitlets >= 4.3.1
BuildRequires:  python3-nbformat >= 4.2.0
# Required for traitlets tests
BuildRequires:  python3-pytest

%description
Interactive HTML widgets for Jupyter notebooks and the IPython kernel.

%package -n     python2-%{pypi_name}
Summary:        IPython HTML widgets for Jupyter
%{?python_provide:%python_provide python2-%{pypi_name}}

Requires:       python2-ipython >= 4.0.0
Requires:       python2-ipykernel >= 4.5.1
Requires:       python2-traitlets >= 4.3.1
Requires:       python2-nbformat >= 4.2.0
Recommends:     python2-widgetsnbextension

%description -n python2-%{pypi_name}
Interactive HTML widgets for Jupyter notebooks and the IPython kernel.

%package -n     python3-%{pypi_name}
Summary:        IPython HTML widgets for Jupyter
%{?python_provide:%python_provide python3-%{pypi_name}}

Requires:       python3-ipython >= 4.0.0
Requires:       python3-ipykernel >= 4.5.1
Requires:       python3-traitlets >= 4.3.1
Requires:       python3-nbformat >= 4.2.0
Recommends:     python3-widgetsnbextension

%description -n python3-%{pypi_name}
Interactive HTML widgets for Jupyter notebooks and the IPython kernel.

%if %{with doc}
%package -n python-%{pypi_name}-doc
Summary:        ipywidgets documentation
%description -n python-%{pypi_name}-doc
Documentation for ipywidgets
%endif


%prep
%autosetup -n %{pypi_name}-%{version}

%build
%py2_build
%py3_build
%if %{with doc}
# generate html docs
sphinx-build docs/source html
# remove the sphinx-build leftovers
rm -rf html/.{doctrees,buildinfo}
%endif


%install
# Must do the subpackages' install first because the scripts in /usr/bin are
# overwritten with every setup.py install.
%py3_install

%py2_install


%check
nosetests-%{python3_version} ipywidgets
nosetests-%{python2_version} ipywidgets


%files -n python2-%{pypi_name}
%license COPYING.md
%doc README.md
%{python2_sitelib}/%{pypi_name}
%{python2_sitelib}/%{pypi_name}-%{version}-py?.?.egg-info

%files -n python3-%{pypi_name}
%license COPYING.md
%doc README.md
%{python3_sitelib}/%{pypi_name}
%{python3_sitelib}/%{pypi_name}-%{version}-py?.?.egg-info

%if %{with doc}
%files -n python-%{pypi_name}-doc
%doc html
%endif


%changelog
* Thu Mar 02 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> - 6.0.0-1
- First non-beta release

* Fri Feb 17 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> - 6.0.0beta10-4
- Really fix bug due to post-version suffix.

* Fri Feb 17 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> - 6.0.0.beta10-3
- Fix bug in setup

* Fri Feb 17 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> - 6.0.0.beta10-2
- New upstream release

* Sun Jan 15 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> - 6.0.0.beta6-1
- New upstream release

* Sun Jan 08 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> - 6.0.0.beta5-2
- Recommend notebook extensions.

* Sat Jan 07 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> - 6.0.0.beta5-1
- Initial package.
