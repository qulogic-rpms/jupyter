# Created by pyp2rpm-3.2.1
%global pypi_name jupyter

Name:           python-%{pypi_name}
Version:        1.0.0
Release:        3%{?dist}
Summary:        Jupyter metapackage. Install all the Jupyter components in one go

License:        BSD
URL:            http://jupyter.org
Source0:        https://files.pythonhosted.org/packages/source/j/%{pypi_name}/%{pypi_name}-%{version}.tar.gz
BuildArch:      noarch

BuildRequires:  python2-devel
BuildRequires:  python-setuptools

BuildRequires:  python3-devel
BuildRequires:  python3-setuptools
BuildRequires:  python3-sphinx

%description
Install the Jupyter system, including the notebook, qtconsole, and the IPython
kernel.


%package -n     python2-%{pypi_name}
Summary:        Jupyter metapackage. Install all the Jupyter components in one go
%{?python_provide:%python_provide python2-%{pypi_name}}

Requires:       python-%{pypi_name}-doc
Requires:       python2-notebook
Requires:       python2-qtconsole
Requires:       python2-jupyter-console
Requires:       python2-nbconvert
Requires:       python2-ipykernel
Requires:       python2-ipywidgets

%description -n python2-%{pypi_name}
Install the Jupyter system, including the notebook, qtconsole, and the IPython
kernel.


%package -n     python3-%{pypi_name}
Summary:        Jupyter metapackage. Install all the Jupyter components in one go
%{?python_provide:%python_provide python3-%{pypi_name}}

Requires:       python-%{pypi_name}-doc
Requires:       python3-notebook
Requires:       python3-qtconsole
Requires:       python3-jupyter-console
Requires:       python3-nbconvert
Requires:       python3-ipykernel
Requires:       python3-ipywidgets

%description -n python3-%{pypi_name}
Install the Jupyter system, including the notebook, qtconsole, and the IPython
kernel.


%package -n python-%{pypi_name}-doc
Summary:        jupyter documentation
%description -n python-%{pypi_name}-doc
Documentation for jupyter


%prep
%autosetup -n %{pypi_name}-%{version}


%build
%py2_build
%py3_build
# generate html docs
sphinx-build-%{python3_version} docs/source html
# remove the sphinx-build leftovers
rm -rf html/.{doctrees,buildinfo}


%install
# Must do the subpackages' install first because the scripts in /usr/bin are
# overwritten with every setup.py install.
%py3_install

%py2_install

# These files are already part of python-jupyter-core.
rm %{buildroot}%{python2_sitelib}/%{pypi_name}.py*
rm %{buildroot}%{python3_sitelib}/__pycache__/*
rm %{buildroot}%{python3_sitelib}/%{pypi_name}.py


%files -n python2-%{pypi_name}
%license LICENSE
%doc README.md

%{python2_sitelib}/%{pypi_name}-%{version}-py?.?.egg-info

%files -n python3-%{pypi_name}
%license LICENSE
%doc README.md
%{python3_sitelib}/%{pypi_name}-%{version}-py?.?.egg-info

%files -n python-%{pypi_name}-doc
%doc html


%changelog
* Mon Jul 24 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> 1.0.0-3
- Rename python-jupyter-notebook as python-notebook.

* Sun Jan 08 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> - 1.0.0-2
- Remove file duplicating jupyter-core.

* Sun Jan 08 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> - 1.0.0-1
- Initial package.
