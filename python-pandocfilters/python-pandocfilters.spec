# Created by pyp2rpm-3.2.1
%global pypi_name pandocfilters

Name:           python-%{pypi_name}
Version:        1.4.1
Release:        1%{?dist}
Summary:        Utilities for writing pandoc filters in python

License:        TODO
URL:            http://github.com/jgm/pandocfilters
Source0:        https://files.pythonhosted.org/packages/source/p/%{pypi_name}/%{pypi_name}-%{version}.tar.gz
BuildArch:      noarch

BuildRequires:  python2-devel
BuildRequires:  python-setuptools

BuildRequires:  python3-devel
BuildRequires:  python3-setuptools

%description
Pandoc filters are pipes that read a JSON serialization of the Pandoc AST from
stdin, transform it in some way, and write it to stdout. They can be used with
pandoc either using pipes or using the --filter (or -F) command-line option.
For more on pandoc filters, see the pandoc documentation under --filter and the
tutorial on writing filters.

%package -n     python2-%{pypi_name}
Summary:        Utilities for writing pandoc filters in python
%{?python_provide:%python_provide python2-%{pypi_name}}

%description -n python2-%{pypi_name}
Pandoc filters are pipes that read a JSON serialization of the Pandoc AST from
stdin, transform it in some way, and write it to stdout. They can be used with
pandoc either using pipes or using the --filter (or -F) command-line option.
For more on pandoc filters, see the pandoc documentation under --filter and the
tutorial on writing filters.

%package -n     python3-%{pypi_name}
Summary:        Utilities for writing pandoc filters in python
%{?python_provide:%python_provide python3-%{pypi_name}}

%description -n python3-%{pypi_name}
Pandoc filters are pipes that read a JSON serialization of the Pandoc AST from
stdin, transform it in some way, and write it to stdout. They can be used with
pandoc either using pipes or using the --filter (or -F) command-line option.
For more on pandoc filters, see the pandoc documentation under --filter and the
tutorial on writing filters.


%prep
%autosetup -n %{pypi_name}-%{version}


%build
%py2_build
%py3_build


%install
# Must do the subpackages' install first because the scripts in /usr/bin are
# overwritten with every setup.py install.
%py3_install

%py2_install


%files -n python2-%{pypi_name}
%license LICENSE
%doc README.rst

%{python2_sitelib}/%{pypi_name}.py*
%{python2_sitelib}/%{pypi_name}-%{version}-py?.?.egg-info

%files -n python3-%{pypi_name}
%license LICENSE
%doc README.rst
%{python3_sitelib}/__pycache__/*
%{python3_sitelib}/%{pypi_name}.py
%{python3_sitelib}/%{pypi_name}-%{version}-py?.?.egg-info


%changelog
* Sun Jan 15 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> - 1.4.1-1
- Initial package.
