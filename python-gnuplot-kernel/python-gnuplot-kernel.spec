%global srcname gnuplot-kernel
%global srcname_ gnuplot_kernel

Name:           python-%{srcname}
Version:        0.2.3
Release:        2%{?dist}
Summary:        A gnuplot kernel for Jupyter

License:        BSD
URL:            https://pypi.python.org/pypi/%{srcname}
Source0:        https://files.pythonhosted.org/packages/source/g/%{srcname_}/%{srcname_}-%{version}.tar.gz
Source1:        %{srcname}-LICENSE

BuildArch:      noarch

BuildRequires:  gnuplot

%global _description \
gnuplot_kernel has been developed for use specifically with Jupyter Notebook. \
It can also be loaded as an IPython extension allowing for gnuplot code in the \
same notebook as python code.

%description %{_description}


%package -n python2-%{srcname}
Summary:        %{summary}
%{?python_provide:%python_provide python2-%{srcname}}

BuildRequires:  python2-devel
BuildRequires:  python2-notebook >= 4.0
BuildRequires:  python2-metakernel >= 0.17.4
BuildRequires:  python2-metakernel-tests >= 0.17.4

BuildRequires:  python2-pytest
BuildRequires:  python2-matplotlib

Requires:       gnuplot
Requires:       python2-notebook >= 4.0
Requires:       python2-metakernel >= 0.17.4

%description -n python2-%{srcname} %{_description}


%package -n python3-%{srcname}
Summary:        %{summary}
%{?python_provide:%python_provide python3-%{srcname}}

BuildRequires:  python3-devel
BuildRequires:  python3-notebook >= 4.0
BuildRequires:  python3-metakernel >= 0.17.4
BuildRequires:  python3-metakernel-tests >= 0.17.4

BuildRequires:  python3-pytest
BuildRequires:  python3-matplotlib

Requires:       gnuplot
Requires:       python3-notebook >= 4.0
Requires:       python3-metakernel >= 0.17.4

%description -n python3-%{srcname} %{_description}


%prep
%autosetup -n %{srcname_}-%{version}
cp %SOURCE1 LICENSE


%build
%py2_build
%py3_build


%install
%py2_install
%py3_install


%check
PYTHONPATH="%{buildroot}%{python3_sitelib}" \
    py.test-%{python3_version} %{srcname_}
PYTHONPATH="%{buildroot}%{python2_sitelib}" \
    py.test-%{python2_version} %{srcname_}


%files -n python2-%{srcname}
%license LICENSE
%doc README.rst
%{python2_sitelib}/%{srcname_}
%{python2_sitelib}/%{srcname_}-%{version}-py?.?.egg-info


%files -n python3-%{srcname}
%license LICENSE
%doc README.rst
%{python3_sitelib}/%{srcname_}
%{python3_sitelib}/%{srcname_}-%{version}-py?.?.egg-info


%changelog
* Mon Jul 24 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> 0.2.3-2
- Official metakernel packages splits out tests.
- Rename python-jupyter-notebook as python-notebook.

* Tue Mar 14 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> 0.2.3-1
- Initial package release.
