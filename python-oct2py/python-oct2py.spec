%global srcname oct2py

Name:           python-%{srcname}
Version:        4.0.6
Release:        1%{?dist}
Summary:        Python to GNU Octave bridge --> run m-files from python

License:        MIT
URL:            https://pypi.python.org/pypi/%{srcname}
Source0:        https://files.pythonhosted.org/packages/source/o/%{srcname}/%{srcname}-%{version}.tar.gz

BuildArch:      noarch

BuildRequires:  ghostscript
BuildRequires:  dos2unix

%global _description \
Oct2Py allows you to seamlessly call M-files and Octave functions from Python. \
It manages the Octave session for you, sharing data behind the scenes using MAT \
files. If you want to run legacy m-files, do not have MATLAB®, and do not fully \
trust a code translator, this is your library.

%description %{_description}


%package -n python2-%{srcname}
Summary:        %{summary}
%{?python_provide:%python_provide python2-%{srcname}}

BuildRequires:  python2-devel
BuildRequires:  python2-octave-kernel >= 0.25
BuildRequires:  python2-scipy >= 0.17
BuildRequires:  python2-numpy >= 1.11

BuildRequires:  python2-pytest
BuildRequires:  python2-nose
BuildRequires:  python2-mock

Requires:       python2-octave-kernel >= 0.25
Requires:       python2-scipy >= 0.17
Requires:       python2-numpy >= 1.11

%description -n python2-%{srcname} %{_description}


%package -n python3-%{srcname}
Summary:        %{summary}
%{?python_provide:%python_provide python3-%{srcname}}

BuildRequires:  python3-devel
BuildRequires:  python3-octave-kernel >= 0.25
BuildRequires:  python3-scipy >= 0.17
BuildRequires:  python3-numpy >= 1.11

BuildRequires:  python3-pytest
BuildRequires:  python3-nose

BuildRequires:  python3-sphinx
BuildRequires:  python3-sphinx-bootstrap-theme
BuildRequires:  python3-numpydoc

Requires:       python3-octave-kernel >= 0.25
Requires:       python3-scipy >= 0.17
Requires:       python3-numpy >= 1.11

%description -n python3-%{srcname} %{_description}


%prep
%autosetup -n %{srcname}-%{version}

dos2unix README.rst
find docs -name '*.rst' -print0 | xargs -0 dos2unix


%build
%py2_build
%py3_build

export PYTHONPATH=$PWD/build/lib/
pushd docs
sphinx-build-%{python3_version} -b html -d _build/doctrees . _build/html
rm _build/html/.buildinfo
popd
find $PYTHONPATH -name '__pycache__' -print0 | xargs -0 rm -rf
unset PYTHONPATH


%install
%py2_install
%py3_install


%check
PYTHONPATH="%{buildroot}%{python3_sitelib}" \
    py.test-%{python3_version} oct2py
PYTHONPATH="%{buildroot}%{python2_sitelib}" \
    py.test-%{python2_version} oct2py


%files -n python2-%{srcname}
%license LICENSE.txt
%doc README.rst docs/_build/html
%{python2_sitelib}/%{srcname}
%{python2_sitelib}/%{srcname}-%{version}-py?.?.egg-info


%files -n python3-%{srcname}
%license LICENSE.txt
%doc README.rst docs/_build/html
%{python3_sitelib}/%{srcname}
%{python3_sitelib}/%{srcname}-%{version}-py?.?.egg-info


%changelog
* Mon Jun 5 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> 4.0.6-1
- New upstream release.

* Mon Mar 13 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> 4.0.3-1
- New upstream release.
- Add nose to BR; needed for NumPy decorators.

* Sun Mar 12 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> 4.0.2-1
- Initial package release.
